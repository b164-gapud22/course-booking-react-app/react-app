import { useContext, useEffect } from 'react'
import { Navigate } from 'react-router-dom';
// Redirect
import UserContext from '../UserContext'



export default function Logout(){

	const { unsetUser, setUser } = useContext(UserContext);

	// Clear the localStorage
	unsetUser()

	// Placing the "setUser" setter function inside of a useEffect is neccessary.
	// By adding useEffect, this will allow the logout page to render first before triggering the useEffect which changes the state of our user
	useEffect(() => {
		// set the user state back into its original value
		setUser({id: null})
	}, [])

	return(

		<Navigate to="/" />

		)



}