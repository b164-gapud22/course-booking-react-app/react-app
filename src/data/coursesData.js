const coursesData = [

	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem, ipsum dolor sit, amet consectetur adipisicing elit. Animi in quasi atque eaque pariatur magnam iste rerum consequuntur, saepe aspernatur obcaecati, hic cum. Inventore molestiae dicta, voluptatum illum. Cupiditate, odio?",
		price:5000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem, ipsum dolor sit, amet consectetur adipisicing elit. Animi in quasi atque eaque pariatur magnam iste rerum consequuntur, saepe aspernatur obcaecati, hic cum. Inventore molestiae dicta, voluptatum illum. Cupiditate, odio?",
		price:50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Lorem, ipsum dolor sit, amet consectetur adipisicing elit. Animi in quasi atque eaque pariatur magnam iste rerum consequuntur, saepe aspernatur obcaecati, hic cum. Inventore molestiae dicta, voluptatum illum. Cupiditate, odio?",
		price:55000,
		onOffer: true
	}


]


export default coursesData;