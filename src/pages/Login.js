import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Navigate } from 'react-router-dom';



export default function Login() {

	// "useContext" hook is used to deconstruct/unpack the data of the UserCOntext object.
	const { user, setUser } = useContext(UserContext);
	
	// User useState to reflect the changes of the state.
	const [ email, setEmail ] = useState("");
	const [ password, setPassword] = useState("");
	const [ isActive, setIsActive ] = useState(false);



	// to show alert for logging in.
	function authenticate(e) {

		// prevent the react from auto refresh
		e.preventDefault();


		/*
		Syntax: 
			fetch("URL", {options})
			.then(res => res.json)
			.then(data => {})
		*/


		fetch('http://localhost:4000/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data.accessToken)

			if(typeof data.accessToken !== "undefined") {
				localStorage.setItem('token', data.accessToken)
				retrieveUserDetails(data.accessToken)

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Zuitt"
				})
			} else {

				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login details and try again!"
				})
			}
		});




		setEmail("");
		setPassword("");


		/*
			Syntax: 
				localStorage.setItem("propertyName", value)
		
		*/
		// localStorage.setItem("email", email)

		// Set the global user state to have properties frok local storage
		// setUser({
		// 	// getItem() getting the email inside the localStorage
		// 	email: localStorage.getItem('email')
		// })


	}


	const retrieveUserDetails = (token) => {

		fetch('http://localhost:4000/api/users/details', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}



	//  to set condition to change the button to active if all requirements are met.
	useEffect(() => {

		if(email !== "" && password !== "") {
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	})


	return(

			// the page will automatically redirect the user  to the courses upon accessing this route "/login"
			(user.id !== null) ?

			<Navigate to="/courses" />

			:

			<Form onSubmit={(e) => {authenticate(e)}}>
			<h1 className="text-center">Log in</h1>
			  <Form.Group 
			  		className="mb-3" 
			  		controlId="user Email">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter email"
			    	value={email}
			    	onChange={e => setEmail(e.target.value)}
			    	required 
			    	/>
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="formBasicPassword">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Password"
			    	value={password}
			    	onChange={e => setPassword(e.target.value)} 
			    	required
			    	/>
			  </Form.Group>
			  <Form.Group className="mb-3" controlId="formBasicCheckbox">
			  </Form.Group>

			  {

			  	isActive?
			  	<Button variant="primary" type="submit">
			  	  Submit
			  	</Button>
			  	:
			  	<Button variant="danger" type="submit" disabled>
			  	  Submit
			  	</Button>

			  }
			  
			</Form>

		)
}